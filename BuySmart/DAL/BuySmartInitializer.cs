﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using BuySmart.Models;

namespace BuySmart.DAL
{
    public class BuySmartInitializer : DropCreateDatabaseIfModelChanges<BuySmartContext>
    {
        protected override void Seed(BuySmartContext context)
        {
            var shops = new List<Shop>
            {
                new Shop{ShopName="Biedronka", ShopType=ShopType.Discount},
                new Shop{ShopName="Lidl", ShopType=ShopType.Discount},
                new Shop{ShopName="Alma", ShopType=ShopType.Delicatessen},
                new Shop{ShopName="Auchan", ShopType=ShopType.HyperMarket},
                new Shop{ShopName="As", ShopType=ShopType.LiquorStore},
                new Shop{ShopName="Tesco", ShopType=ShopType.HyperMarket},
                new Shop{ShopName="Real", ShopType=ShopType.HyperMarket},
                new Shop{ShopName="Carrefour", ShopType=ShopType.HyperMarket},
                new Shop{ShopName="Makro", ShopType=ShopType.HyperMarket},
                new Shop{ShopName="Aldi", ShopType=ShopType.Discount},
                new Shop{ShopName="Netto", ShopType=ShopType.Discount},
                new Shop{ShopName="InterMarche", ShopType=ShopType.SuperMarket},
                new Shop{ShopName="Grosik", ShopType=ShopType.UsualShop},
                new Shop{ShopName="ABC", ShopType=ShopType.UsualShop},
                new Shop{ShopName="Polo Market", ShopType=ShopType.SuperMarket},
                new Shop{ShopName="Piotr i Paweł", ShopType=ShopType.Delicatessen},
                new Shop{ShopName="Społem", ShopType=ShopType.UsualShop},
                new Shop{ShopName="Eko", ShopType=ShopType.SuperMarket},
                new Shop{ShopName="Żabka", ShopType=ShopType.UsualShop},
                new Shop{ShopName="FreshMarket", ShopType=ShopType.SuperMarket},
                new Shop{ShopName="Dino", ShopType=ShopType.SuperMarket},
                new Shop{ShopName="Rossmann", ShopType=ShopType.DrugStore},
                new Shop{ShopName="MarcPol", ShopType=ShopType.SuperMarket},
                new Shop{ShopName="Małpka Express", ShopType=ShopType.LiquorStore},
                new Shop{ShopName="E.Leclerc", ShopType=ShopType.HyperMarket},
                new Shop{ShopName="Kaufland", ShopType=ShopType.HyperMarket},
                new Shop{ShopName="Chata Polska", ShopType=ShopType.UsualShop},
                new Shop{ShopName="Lewiatan", ShopType=ShopType.SuperMarket},
                new Shop{ShopName="Delikatesy Centrum", ShopType=ShopType.Delicatessen},
                new Shop{ShopName="Spar", ShopType=ShopType.UsualShop},
                new Shop{ShopName="Stokrotka", ShopType=ShopType.UsualShop},
                new Shop{ShopName="Aldik", ShopType=ShopType.SuperMarket},
                new Shop{ShopName="Groszek", ShopType=ShopType.UsualShop},
                new Shop{ShopName="Drogeria Natura", ShopType=ShopType.DrugStore},
                new Shop{ShopName="Hebe", ShopType=ShopType.DrugStore},
                new Shop{ShopName="Drogerie Polskie", ShopType=ShopType.DrugStore},
                new Shop{ShopName="Jasmin", ShopType=ShopType.DrugStore},
                new Shop{ShopName="Koliber", ShopType=ShopType.DrugStore},
                new Shop{ShopName="Słoneczko", ShopType=ShopType.UsualShop},
            };

            shops.ForEach(s => context.Shops.Add(s));
            context.SaveChanges();

            var addresses = new List<Address>
            {
                
                new Address{ShopId=1,City="Świebodzin",Street="Łużycka 33A"},
                new Address{ShopId=1,City="Zielona Góra",Street="Podgórna 43D"},

                new Address{ShopId=2,City="Świebodzin",Street="gen. Władysława Sikorskiego 21"},
                new Address{ShopId=2,City="Zielona Góra",Street="Staszica 5"},

                new Address{ShopId=3,City="Zielona Góra",Street="Wrocławska 17"},
                new Address{ShopId=3,City="Poznań",Street="Półwiejska 42"},
                new Address{ShopId=3,City="Szczecin",Street="Aleja Niepodległości 36"},

                new Address{ShopId=4,City="Zielona Góra",Street="Stefana Batorego 128"},
                new Address{ShopId=4,City="Poznań",Street="Głogowska 432"},

                new Address{ShopId=6,City="Zielona Góra",Street="Energetyków 2A"},
                new Address{ShopId=6,City="Poznań",Street="Opieńskiego 1"},


            };

            addresses.ForEach(s => context.Addresses.Add(s));
            context.SaveChanges();

            var products = new List<Product>
            {
                new Product { UnitOfMeasure=UnitOfMeasure.Item, ProductDescription = "kiełbasa wieprzowa wędzona 200g" },
                new Product { UnitOfMeasure=UnitOfMeasure.Item, ProductDescription = "ser żółty półtłusty plastry 300g" },
                new Product { UnitOfMeasure=UnitOfMeasure.Kilogram, ProductDescription = "ziemniaki" },
                new Product { UnitOfMeasure=UnitOfMeasure.Kilogram, ProductDescription = "cebula" },
                new Product { UnitOfMeasure=UnitOfMeasure.Kilogram, ProductDescription = "kiwi" },
            };

            products.ForEach(s => context.Products.Add(s));
            context.SaveChanges();

            var productOccurences = new List<ProductOccurence>
            {
                //do testowania ceny koszyka
                new ProductOccurence{ProductId=1, ShopId=1, ProductPrice = 3.55M, ProductBuyDate = DateTime.Parse("2016-08-08"), IsPromotional = false },
                new ProductOccurence{ProductId=1, ShopId=2, ProductPrice = 3.99M, ProductBuyDate = DateTime.Parse("2016-09-09"), IsPromotional = false },
                new ProductOccurence{ProductId=1, ShopId=3, ProductPrice = 4.49M, ProductBuyDate = DateTime.Parse("2016-10-10"), IsPromotional = false },
                new ProductOccurence{ProductId=1, ShopId=4, ProductPrice = 3.80M, ProductBuyDate = DateTime.Parse("2016-11-11"), IsPromotional = false },
                new ProductOccurence{ProductId=1, ShopId=6, ProductPrice = 3.69M, ProductBuyDate = DateTime.Parse("2016-12-12"), IsPromotional = false },

                //do testowania lokalizacji
                new ProductOccurence{ProductId=2, ShopId=1, ProductPrice = 5.20M, ProductBuyDate = DateTime.Parse("2016-03-02"), IsPromotional = false },
                new ProductOccurence{ProductId=2, ShopId=1, ProductPrice = 5.39M, ProductBuyDate = DateTime.Parse("2016-11-19"), IsPromotional = true }, 
                new ProductOccurence{ProductId=2, ShopId=1, ProductPrice = 5.39M, ProductBuyDate = DateTime.Parse("2016-11-24"), IsPromotional = false },
                new ProductOccurence{ProductId=2, ShopId=2, ProductPrice = 4.99M, ProductBuyDate = DateTime.Parse("2016-05-28"), IsPromotional = false },
                new ProductOccurence{ProductId=2, ShopId=2, ProductPrice = 5.19M, ProductBuyDate = DateTime.Parse("2016-10-13"), IsPromotional = false },

                //do testowania ceny koszyka
                new ProductOccurence{ProductId=3, ShopId=1, ProductPrice = 1.29M, ProductBuyDate = DateTime.Parse("2016-12-20"), IsPromotional = false },
                new ProductOccurence{ProductId=3, ShopId=2, ProductPrice = 1.39M, ProductBuyDate = DateTime.Parse("2016-12-22"), IsPromotional = false },
                new ProductOccurence{ProductId=3, ShopId=3, ProductPrice = 1.89M, ProductBuyDate = DateTime.Parse("2017-07-08"), IsPromotional = false },
                new ProductOccurence{ProductId=3, ShopId=4, ProductPrice = 1.19M, ProductBuyDate = DateTime.Parse("2016-01-02"), IsPromotional = false },
                new ProductOccurence{ProductId=3, ShopId=6, ProductPrice = 1.09M, ProductBuyDate = DateTime.Parse("2017-01-17"), IsPromotional = false },

                //do testowania inflacji
                new ProductOccurence{ProductId=4, ShopId=1, ProductPrice = 2.69M, ProductBuyDate = DateTime.Parse("2013-08-29"), IsPromotional = false },
                new ProductOccurence{ProductId=4, ShopId=1, ProductPrice = 2.89M, ProductBuyDate = DateTime.Parse("2015-08-29"), IsPromotional = false },
                new ProductOccurence{ProductId=4, ShopId=2, ProductPrice = 3.09M, ProductBuyDate = DateTime.Parse("2014-06-19"), IsPromotional = false },
                new ProductOccurence{ProductId=4, ShopId=2, ProductPrice = 3.29M, ProductBuyDate = DateTime.Parse("2016-06-19"), IsPromotional = false },
                new ProductOccurence{ProductId=4, ShopId=3, ProductPrice = 3.59M, ProductBuyDate = DateTime.Parse("2015-11-02"), IsPromotional = false },
                new ProductOccurence{ProductId=4, ShopId=3, ProductPrice = 3.89M, ProductBuyDate = DateTime.Parse("2017-01-02"), IsPromotional = false },
                new ProductOccurence{ProductId=4, ShopId=4, ProductPrice = 2.59M, ProductBuyDate = DateTime.Parse("2015-04-01"), IsPromotional = false },
                new ProductOccurence{ProductId=4, ShopId=4, ProductPrice = 2.69M, ProductBuyDate = DateTime.Parse("2016-04-01"), IsPromotional = false },


                new ProductOccurence{ProductId=3, ShopId=1, ProductPrice = 1.23M, ProductBuyDate = DateTime.Parse("2013-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=1, ProductPrice = 1.25M, ProductBuyDate = DateTime.Parse("2014-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=1, ProductPrice = 1.27M, ProductBuyDate = DateTime.Parse("2015-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=1, ProductPrice = 1.29M, ProductBuyDate = DateTime.Parse("2016-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=1, ProductPrice = 1.39M, ProductBuyDate = DateTime.Parse("2017-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=2, ProductPrice = 1.39M, ProductBuyDate = DateTime.Parse("2016-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=2, ProductPrice = 1.49M, ProductBuyDate = DateTime.Parse("2017-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=3, ProductPrice = 1.89M, ProductBuyDate = DateTime.Parse("2016-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=3, ProductPrice = 2.09M, ProductBuyDate = DateTime.Parse("2017-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=4, ProductPrice = 1.19M, ProductBuyDate = DateTime.Parse("2016-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=4, ProductPrice = 1.24M, ProductBuyDate = DateTime.Parse("2017-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=6, ProductPrice = 1.09M, ProductBuyDate = DateTime.Parse("2016-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=3, ShopId=6, ProductPrice = 1.19M, ProductBuyDate = DateTime.Parse("2017-01-05"), IsPromotional = true },


                new ProductOccurence{ProductId=4, ShopId=1, ProductPrice = 2.54M, ProductBuyDate = DateTime.Parse("2013-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=1, ProductPrice = 2.59M, ProductBuyDate = DateTime.Parse("2014-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=1, ProductPrice = 2.64M, ProductBuyDate = DateTime.Parse("2015-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=1, ProductPrice = 2.69M, ProductBuyDate = DateTime.Parse("2016-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=1, ProductPrice = 2.99M, ProductBuyDate = DateTime.Parse("2017-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=2, ProductPrice = 2.89M, ProductBuyDate = DateTime.Parse("2016-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=2, ProductPrice = 2.99M, ProductBuyDate = DateTime.Parse("2017-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=3, ProductPrice = 3.19M, ProductBuyDate = DateTime.Parse("2016-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=3, ProductPrice = 3.49M, ProductBuyDate = DateTime.Parse("2017-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=4, ProductPrice = 2.59M, ProductBuyDate = DateTime.Parse("2016-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=4, ProductPrice = 2.79M, ProductBuyDate = DateTime.Parse("2017-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=6, ProductPrice = 2.49M, ProductBuyDate = DateTime.Parse("2016-01-05"), IsPromotional = true },
                new ProductOccurence{ProductId=4, ShopId=6, ProductPrice = 2.54M, ProductBuyDate = DateTime.Parse("2017-01-05"), IsPromotional = true },

                //do testowania siły nabywczej

                new ProductOccurence{ProductId=5, ShopId=1, ProductPrice = 6.33M, ProductBuyDate = DateTime.Parse("2013-05-06"), IsPromotional = false },
                new ProductOccurence{ProductId=5, ShopId=1, ProductPrice = 6.36M, ProductBuyDate = DateTime.Parse("2014-05-06"), IsPromotional = false },
                new ProductOccurence{ProductId=5, ShopId=1, ProductPrice = 6.46M, ProductBuyDate = DateTime.Parse("2015-11-09"), IsPromotional = false },
                new ProductOccurence{ProductId=5, ShopId=1, ProductPrice = 6.56M, ProductBuyDate = DateTime.Parse("2016-02-03"), IsPromotional = false },
                new ProductOccurence{ProductId=5, ShopId=2, ProductPrice = 6.89M, ProductBuyDate = DateTime.Parse("2016-07-05"), IsPromotional = false },
                new ProductOccurence{ProductId=5, ShopId=2, ProductPrice = 7.09M, ProductBuyDate = DateTime.Parse("2017-01-01"), IsPromotional = true },

                new ProductOccurence{ProductId=5, ShopId=3, ProductPrice = 8.49M, ProductBuyDate = DateTime.Parse("2013-10-10"), IsPromotional = false },
                new ProductOccurence{ProductId=5, ShopId=3, ProductPrice = 8.59M, ProductBuyDate = DateTime.Parse("2014-09-09"), IsPromotional = false },
                new ProductOccurence{ProductId=5, ShopId=3, ProductPrice = 8.99M, ProductBuyDate = DateTime.Parse("2015-08-08"), IsPromotional = false },
                new ProductOccurence{ProductId=5, ShopId=4, ProductPrice = 6.69M, ProductBuyDate = DateTime.Parse("2016-07-07"), IsPromotional = false },
                new ProductOccurence{ProductId=5, ShopId=4, ProductPrice = 6.99M, ProductBuyDate = DateTime.Parse("2017-01-09"), IsPromotional = true },

                new ProductOccurence{ProductId=5, ShopId=6, ProductPrice = 6.49M, ProductBuyDate = DateTime.Parse("2016-02-08"), IsPromotional = false },
                new ProductOccurence{ProductId=5, ShopId=6, ProductPrice = 6.52M, ProductBuyDate = DateTime.Parse("2016-03-01"), IsPromotional = true },
            };

            productOccurences.ForEach(s => context.ProductOccurences.Add(s));
            context.SaveChanges();
        }
    }
}