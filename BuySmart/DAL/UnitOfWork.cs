﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuySmart.Models;

namespace BuySmart.DAL
{
    public class UnitOfWork : IDisposable
    {
        private BuySmartContext context = new BuySmartContext();
        private GenericRepository<Product> _productRepository;
        private GenericRepository<ProductOccurence> _productOccurenceRepository;
        private GenericRepository<Shop> _shopRepository;
        private GenericRepository<Address> _addressRepository;
        private GenericRepository<ProductPhoto> _productPhotoRepository;
        private GenericRepository<ShopPhoto> _shopPhotoRepository;

        public GenericRepository<Product> ProductRepository
        {
            get
            {
                if (this._productRepository == null)
                {
                    this._productRepository = new GenericRepository<Product>(context);
                }
                return _productRepository;
            }
        }

        public GenericRepository<ProductOccurence> ProductOccurenceRepository
        {
            get
            {
                if (this._productOccurenceRepository == null)
                {
                    this._productOccurenceRepository = new GenericRepository<ProductOccurence>(context);
                }
                return _productOccurenceRepository;
            }
        }

        public GenericRepository<Shop> ShopRepository
        {
            get
            {
                if (this._shopRepository == null)
                {
                    this._shopRepository = new GenericRepository<Shop>(context);
                }
                return _shopRepository;
            }
        }

        public GenericRepository<Address> AddressRepository
        {
            get
            {
                if (this._addressRepository == null)
                {
                    this._addressRepository = new GenericRepository<Address>(context);
                }
                return _addressRepository;
            }
        }

        public GenericRepository<ProductPhoto> ProductPhotoRepository
        {
            get
            {
                if (this._productPhotoRepository == null)
                {
                    this._productPhotoRepository = new GenericRepository<ProductPhoto>(context);
                }
                return _productPhotoRepository;
            }
        }

        public GenericRepository<ShopPhoto> ShopPhotoRepository
        {
            get
            {
                if (this._shopPhotoRepository == null)
                {
                    this._shopPhotoRepository = new GenericRepository<ShopPhoto>(context);
                }
                return _shopPhotoRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}