﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using BuySmart.Models;

namespace BuySmart.DAL
{
    public class BuySmartContext : DbContext
    {

        public BuySmartContext() : base("BuySmartContext")
        {

        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductOccurence> ProductOccurences { get; set; }
        public DbSet<ProductPhoto> ProductPhotos { get; set; }
        public DbSet<ShopPhoto> ShopPhotos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}