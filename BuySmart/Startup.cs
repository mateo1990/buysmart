﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BuySmart.Startup))]
namespace BuySmart
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
