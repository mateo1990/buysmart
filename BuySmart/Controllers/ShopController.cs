﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BuySmart.DAL;
using BuySmart.Models;
using BuySmart.ViewModels;

namespace BuySmart.Controllers
{
    public class ShopController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public IEnumerable<Shop> GetShops(ShopSearchModel searchModel)
        {
            var result = unitOfWork.ShopRepository.Get(includeProperties: "Photo,ProductOccurences,Addresses");

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.Name))
                    result = result.Where(x => x.ShopName.Contains(searchModel.Name));
                if (searchModel.ShopType.HasValue)
                    result = result.Where(x => x.ShopType.Equals(searchModel.ShopType));
            }
            return result;
        }
        
        // GET: Shop
        public ViewResult Index(string sortOrder, ShopSearchModel searchModel, string currentFilterShopName,
            ShopType? currentFilterShopType, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.IDSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.NameSortParm = sortOrder == "name_asc" ? "name_desc" : "name_asc";
            ViewBag.TypeSortParm = sortOrder == "type_asc" ? "type_desc" : "type_asc";

            if ((searchModel.Name != null) || (searchModel.ShopType != null))
            {
                page = 1;
            }
            else
            {
                searchModel.Name = currentFilterShopName;
                searchModel.ShopType = currentFilterShopType;
            }

            var shops = GetShops(searchModel);

            ViewBag.CurrentFilterShopName = searchModel.Name;
            ViewBag.CurrentFilterShopType = searchModel.ShopType;

            switch (sortOrder)
            {
                case "id_desc":
                    shops = shops.OrderByDescending(s => s.ShopId);
                    break;
                case "name_desc":
                    shops = shops.OrderByDescending(s => s.ShopName);
                    break;
                case "name_asc":
                    shops = shops.OrderBy(s => s.ShopName);
                    break;
                case "type_desc":
                    shops = shops.OrderByDescending(s => s.ShopType.ToString());
                    break;
                case "type_asc":
                    shops = shops.OrderBy(s => s.ShopType.ToString());
                    break;
                default:
                    shops = shops.OrderBy(s => s.ShopId);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            ShopIndexModel shopIndexModel = new ShopIndexModel(shops, pageNumber, pageSize, currentFilterShopType);
            return View(shopIndexModel);
        }

        public ViewResult FetchProductOccurences(int id)
        {
            var products = unitOfWork.ProductOccurenceRepository.Get(p => p.ShopId == id, includeProperties: "Product,Shop");
            ViewBag.ShopName = unitOfWork.ShopRepository.GetById(id).ShopName;
            return View(products.ToList());
        }

        public ViewResult FetchAddresses(int id)
        {
            var addresses = unitOfWork.AddressRepository.Get(p => p.ShopId == id, includeProperties: "Shop");
            ViewBag.ShopName = unitOfWork.ShopRepository.GetById(id).ShopName;
            return View(addresses.ToList());
        }

        // GET: Shop/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = unitOfWork.ShopRepository.GetById(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        // GET: Shop/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Shop/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShopName,ShopType")]Shop shop, HttpPostedFileBase upload)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (upload != null && upload.ContentLength > 0)
                    {
                        var avatar = new ShopPhoto
                        {
                            FileName = System.IO.Path.GetFileName(upload.FileName),
                            ContentType = upload.ContentType
                        };
                        using (var reader = new System.IO.BinaryReader(upload.InputStream))
                        {
                            avatar.Content = reader.ReadBytes(upload.ContentLength);
                        }
                        shop.Photo = avatar;
                    }
                    unitOfWork.ShopRepository.Insert(shop);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Zapisanie zmian zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.");
            }
            return View(shop);
        }

        // GET: Shop/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = unitOfWork.ShopRepository.GetById(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        // POST: Shop/Edit/5
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id, HttpPostedFileBase upload)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var shopToUpdate = unitOfWork.ShopRepository.GetById(id);
            if (TryUpdateModel(shopToUpdate, "",
               new string[] { "Shopname", "ShopType" }))
            {
                try
                {
                    if (upload != null && upload.ContentLength > 0)
                    {
                        if (shopToUpdate.Photo != null)
                        {
                            unitOfWork.ShopPhotoRepository.Delete(shopToUpdate.Photo);
                        }
                        var avatar = new ShopPhoto
                        {
                            FileName = System.IO.Path.GetFileName(upload.FileName),
                            ContentType = upload.ContentType
                        };
                        using (var reader = new System.IO.BinaryReader(upload.InputStream))
                        {
                            avatar.Content = reader.ReadBytes(upload.ContentLength);
                        }
                        shopToUpdate.Photo = avatar;
                    }
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
                catch (DataException)
                {
                    ModelState.AddModelError("", "Zapisanie zmian zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.");
                }
            }
            return View(shopToUpdate);
        }

        // GET: Shop/Delete/5
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Usuwanie zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.";
            }
            Shop shop = unitOfWork.ShopRepository.GetById(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        // POST: Shop/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Shop shop = unitOfWork.ShopRepository.GetById(id);
                if (shop.Photo != null)
                {
                    ShopPhoto shopPhoto = unitOfWork.ShopPhotoRepository.GetById(shop.ShopId);
                    unitOfWork.ShopPhotoRepository.Delete(shopPhoto.ShopPhotoId);
                }
                unitOfWork.ShopRepository.Delete(id);
                unitOfWork.Save();
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
