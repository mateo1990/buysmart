﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using BuySmart.DAL;
using BuySmart.Models;
using BuySmart.ViewModels;

namespace BuySmart.Controllers
{
    public class ProductOccurenceController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        private void PopulateProductsDropDownList(object selectedProduct = null)
        {
            var productQuery = unitOfWork.ProductRepository.Get(
                orderBy: q => q.OrderBy(d => d.ProductId));
            ViewBag.ProductID = new SelectList(productQuery, "ProductId", "ProductDescription", selectedProduct);
        }

        private void PopulateShopsDropDownList(object selectedShop = null)
        {
            var shopQuery = unitOfWork.ShopRepository.Get(
                orderBy: q => q.OrderBy(d => d.ShopId));
            ViewBag.ShopID = new SelectList(shopQuery, "ShopId", "ShopName", selectedShop);
        }

        public IEnumerable<ProductOccurence> GetProductOccurences(ProductOccurenceSearchModel searchModel)
        {
            var result = unitOfWork.ProductOccurenceRepository.Get(includeProperties: "Product,Shop");
            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.Description))
                    result = result.Where(x => x.Product.ProductDescription.Contains(searchModel.Description));
                if (!string.IsNullOrEmpty(searchModel.ShopName))
                    result = result.Where(x => x.Shop.ShopName.Contains(searchModel.ShopName));
                if (searchModel.PriceFrom.HasValue)
                    result = result.Where(x => x.ProductPrice >= searchModel.PriceFrom);
                if (searchModel.PriceTo.HasValue)
                    result = result.Where(x => x.ProductPrice <= searchModel.PriceTo);
                if (searchModel.BuyDateFrom.HasValue)
                    result = result.Where(x => x.ProductBuyDate >= searchModel.BuyDateFrom);
                if (searchModel.BuyDateTo.HasValue)
                    result = result.Where(x => x.ProductBuyDate <= searchModel.BuyDateTo);
                if (searchModel.IsPromotional == true)
                    result = result.Where(x => x.IsPromotional == true);
            }
            return result;
        }

        // GET: ProductOccurence
        public ViewResult Index(string sortOrder, ProductOccurenceSearchModel searchModel, 
            string currentFilterDescription, string currentFilterShopName, decimal? currentFilterPriceFrom, decimal? currentFilterPriceTo,
            DateTime? currentFilterBuyDateFrom, DateTime? currentFilterBuyDateTo, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.IDSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.ProductSortParm = sortOrder == "product_asc" ? "product_desc" : "product_asc";
            ViewBag.ShopSortParm = sortOrder == "shop_asc" ? "shop_desc" : "shop_asc";
            ViewBag.PriceSortParm = sortOrder == "price_asc" ? "price_desc" : "price_asc";
            ViewBag.BuyDateSortParm = sortOrder == "buyDate_asc" ? "buyDate_desc" : "buyDate_asc";

            if ((searchModel.Description != null) || (searchModel.ShopName != null)
                || (searchModel.PriceFrom != null) || (searchModel.PriceTo != null)
                || (searchModel.BuyDateFrom != null) || (searchModel.BuyDateTo != null))
            {
                page = 1;
            }
            else
            {
                searchModel.Description = currentFilterDescription;
                searchModel.ShopName = currentFilterShopName;
                searchModel.PriceFrom = currentFilterPriceFrom;
                searchModel.PriceTo = currentFilterPriceTo;
                searchModel.BuyDateFrom = currentFilterBuyDateFrom;
                searchModel.BuyDateTo = currentFilterBuyDateTo;
            }

            var productOccurences = GetProductOccurences(searchModel);

            ViewBag.CurrentFilterDescription = searchModel.Description;
            ViewBag.CurrentFilterShopName = searchModel.ShopName;
            ViewBag.CurrentFilterPriceFrom = searchModel.PriceFrom;
            ViewBag.CurrentFilterPriceTo = searchModel.PriceTo;
            ViewBag.CurrentFilterBuyDateFrom = String.Format("{0:yyyy-MM-dd}", searchModel.BuyDateFrom);
            ViewBag.CurrentFilterBuyDateTo = String.Format("{0:yyyy-MM-dd}", searchModel.BuyDateTo);

            switch (sortOrder)
            {
                case "id_desc":
                    productOccurences = productOccurences.OrderByDescending(s => s.ProductOccurenceId);
                    break;
                case "product_desc":
                    productOccurences = productOccurences.OrderByDescending(s => s.Product.ProductDescription);
                    break;
                case "product_asc":
                    productOccurences = productOccurences.OrderBy(s => s.Product.ProductDescription);
                    break;
                case "shop_desc":
                    productOccurences = productOccurences.OrderByDescending(s => s.Shop.ShopName);
                    break;
                case "shop_asc":
                    productOccurences = productOccurences.OrderBy(s => s.Shop.ShopName);
                    break;
                case "price_desc":
                    productOccurences = productOccurences.OrderByDescending(s => s.ProductPrice);
                    break;
                case "price_asc":
                    productOccurences = productOccurences.OrderBy(s => s.ProductPrice);
                    break;
                case "buyDate_desc":
                    productOccurences = productOccurences.OrderByDescending(s => s.ProductBuyDate);
                    break;
                case "buyDate_asc":
                    productOccurences = productOccurences.OrderBy(s => s.ProductBuyDate);
                    break;
                default:
                    productOccurences = productOccurences.OrderBy(s => s.ProductOccurenceId);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            ProductOccurenceIndexModel productOccurenceIndexModel = new ProductOccurenceIndexModel(productOccurences, pageNumber, pageSize,
                currentFilterPriceFrom, currentFilterPriceTo, currentFilterBuyDateFrom, currentFilterBuyDateTo);
            return View(productOccurenceIndexModel);
        }

        // GET: ProductOccurence/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductOccurence productOccurence = unitOfWork.ProductOccurenceRepository.GetById(id);

            if (productOccurence == null)
            {
                return HttpNotFound();
            }
            return View(productOccurence);
        }

        // GET: ProductOccurence/Create
        public ActionResult Create()
        {
            PopulateProductsDropDownList();
            PopulateShopsDropDownList();
            return View();
        }

        // POST: ProductOccurence/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductId,ShopId,ProductPrice,ProductBuyDate,IsPromotional")]ProductOccurence productOccurence)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.ProductOccurenceRepository.Insert(productOccurence);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
                PopulateProductsDropDownList(productOccurence.ProductId);
                PopulateShopsDropDownList(productOccurence.ShopId);
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Zapisanie zmian zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.");
            }
            return View(productOccurence);
        }

        // GET: ProductOccurence/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductOccurence productOccurence = unitOfWork.ProductOccurenceRepository.GetById(id);
            if (productOccurence == null)
            {
                return HttpNotFound();
            }
            PopulateProductsDropDownList(productOccurence.ProductId);
            PopulateShopsDropDownList(productOccurence.ShopId);
            return View(productOccurence);
        }

        // POST: ProductOccurence/Edit/5
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var productOccurenceToUpdate = unitOfWork.ProductOccurenceRepository.GetById(id);
            PopulateProductsDropDownList(productOccurenceToUpdate.ProductId);
            PopulateShopsDropDownList(productOccurenceToUpdate.ShopId);
            if (TryUpdateModel(productOccurenceToUpdate, "",
               new string[] { "ProductId", "ShopId", "ProductPrice", "ProductBuyDate", "IsPromotional" }))
            {
                try
                {
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
                catch (DataException)
                {
                    ModelState.AddModelError("", "Zapisanie zmian zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.");
                }
            }
            return View(productOccurenceToUpdate);
        }

        // GET: ProductOccurence/Delete/5
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Usuwanie zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.";
            }
            ProductOccurence productOccurence = unitOfWork.ProductOccurenceRepository.GetById(id);
            if (productOccurence == null)
            {
                return HttpNotFound();
            }
            return View(productOccurence);
        }

        // POST: ProductOccurence/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductOccurence productOccurence = unitOfWork.ProductOccurenceRepository.GetById(id);
            unitOfWork.ProductOccurenceRepository.Delete(productOccurence);
            unitOfWork.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
