﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BuySmart.DAL;
using BuySmart.Models;
using BuySmart.ViewModels;

namespace BuySmart.Controllers
{
    public class ProductController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public IEnumerable<Product> GetProducts(ProductSearchModel searchModel)
        {
            var result = unitOfWork.ProductRepository.Get(includeProperties: "Photo,ProductOccurences");

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.Description))
                    result = result.Where(x => x.ProductDescription.Contains(searchModel.Description));
                if (searchModel.UnitOfMeasure.HasValue)
                    result = result.Where(x => x.UnitOfMeasure.Equals(searchModel.UnitOfMeasure));
            }
            return result;
        }

        // GET: Product
        public ViewResult Index(string sortOrder, ProductSearchModel searchModel, string currentFilterDescription,
            UnitOfMeasure? currentFilterUnitOfMeasure, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.IDSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.DescriptionSortParm = sortOrder == "description_asc" ? "description_desc" : "description_asc";
            ViewBag.UnitOfMeasureSortParm = sortOrder == "unitOfMeasure_asc" ? "unitOfMeasure_desc" : "unitOfMeasure_asc";

            if ((searchModel.Description != null) || (searchModel.UnitOfMeasure != null))
            {
                page = 1;
            }
            else
            {
                searchModel.Description = currentFilterDescription;
                searchModel.UnitOfMeasure = currentFilterUnitOfMeasure;
            }

            var products = GetProducts(searchModel);

            ViewBag.CurrentFilterDescription = searchModel.Description;
            ViewBag.CurrentFilterUnitOfMeasure = searchModel.UnitOfMeasure;

            switch (sortOrder)
            {
                case "id_desc":
                    products = products.OrderByDescending(s => s.ProductId);
                    break;
                case "description_desc":
                    products = products.OrderByDescending(s => s.ProductDescription);
                    break;
                case "description_asc":
                    products = products.OrderBy(s => s.ProductDescription);
                    break;
                case "unitOfMeasure_desc":
                    products = products.OrderByDescending(s => s.UnitOfMeasure);
                    break;
                case "unitOfMeasure_asc":
                    products = products.OrderBy(s => s.UnitOfMeasure);
                    break;
                default:
                    products = products.OrderBy(s => s.ProductId);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            ProductIndexModel productIndexModel = new ProductIndexModel(products, pageNumber, pageSize, currentFilterUnitOfMeasure);
            return View(productIndexModel);
        }

        public ViewResult FetchProductOccurences(int id)
        {
            var products = unitOfWork.ProductOccurenceRepository.Get(p => p.ProductId == id, includeProperties: "Product,Shop");
            ViewBag.ProductDescription = unitOfWork.ProductRepository.GetById(id).ProductDescription;
            return View(products.ToList());
        }

        // GET: Product/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = unitOfWork.ProductRepository.GetById(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UnitOfMeasure,ProductDescription")]Product product, HttpPostedFileBase upload)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (upload != null && upload.ContentLength > 0)
                    {
                        var avatar = new ProductPhoto
                        {
                            FileName = System.IO.Path.GetFileName(upload.FileName),
                            ContentType = upload.ContentType
                        };
                        using (var reader = new System.IO.BinaryReader(upload.InputStream))
                        {
                            avatar.Content = reader.ReadBytes(upload.ContentLength);
                        }
                        product.Photo = avatar;
                    }
                    unitOfWork.ProductRepository.Insert(product);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Zapisanie zmian zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.");
            }
            return View(product);
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = unitOfWork.ProductRepository.GetById(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Product/Edit/5
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id, HttpPostedFileBase upload)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var productToUpdate = unitOfWork.ProductRepository.GetById(id);
            if (TryUpdateModel(productToUpdate, "",
               new string[] { "UnitOfMeasure", "ProductDescription" }))
            {
                try
                {
                    if (upload != null && upload.ContentLength > 0)
                    {
                        if (productToUpdate.Photo != null)
                        {
                            unitOfWork.ProductPhotoRepository.Delete(productToUpdate.Photo);
                        }
                        var avatar = new ProductPhoto
                        {
                            FileName = System.IO.Path.GetFileName(upload.FileName),
                            ContentType = upload.ContentType
                        };
                        using (var reader = new System.IO.BinaryReader(upload.InputStream))
                        {
                            avatar.Content = reader.ReadBytes(upload.ContentLength);
                        }
                        productToUpdate.Photo = avatar;
                    }
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
                catch (DataException)
                {
                    ModelState.AddModelError("", "Zapisanie zmian zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.");
                }
            }
            return View(productToUpdate);
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Usuwanie zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.";
            }
            Product product = unitOfWork.ProductRepository.GetById(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Product/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Product product = unitOfWork.ProductRepository.GetById(id);
                if (product.Photo != null)
                {
                    ProductPhoto productPhoto = unitOfWork.ProductPhotoRepository.GetById(product.ProductId);
                    unitOfWork.ProductPhotoRepository.Delete(productPhoto.ProductPhotoId);
                }
                unitOfWork.ProductRepository.Delete(id);
                unitOfWork.Save();
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}