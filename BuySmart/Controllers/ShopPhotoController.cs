﻿using BuySmart.DAL;
using System.Web.Mvc;

namespace BuySmart.Controllers
{
    public class ShopPhotoController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        //
        // GET: /File/
        public ActionResult Index(int id)
        {
            var fileToRetrieve = unitOfWork.ShopPhotoRepository.GetById(id);
            return File(fileToRetrieve.Content, fileToRetrieve.ContentType);
        }
    }
}