﻿using System.Web.Mvc;
using BuySmart.DAL;

namespace BuySmart.Controllers
{
    public class ProductPhotoController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        //
        // GET: /File/
        public ActionResult Index(int id)
        {
            var fileToRetrieve = unitOfWork.ProductPhotoRepository.GetById(id);
            return File(fileToRetrieve.Content, fileToRetrieve.ContentType);
        }
    }
}