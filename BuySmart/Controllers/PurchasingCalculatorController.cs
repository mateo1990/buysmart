﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BuySmart.DAL;
using BuySmart.Models;
using BuySmart.ViewModels.PurchasingCalculator;

namespace BuySmart.Controllers
{
    public class PurchasingCalculatorController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        [HttpGet]
        public ActionResult SelectCriteria()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SelectCriteria(PurchasingCalculatorCriteriaModel criteriaModel)
        {
            TempData["currentCriteria"] = criteriaModel;
            if ((criteriaModel.Localization == null) || (criteriaModel.DateFrom == null) ||
                (criteriaModel.DateTo == null))
                return View("ErrorCriteria");
            return RedirectToAction("SelectProducts", criteriaModel);
        }

        [HttpGet]
        public ActionResult SelectProducts()
        {
            var allProducts = unitOfWork.ProductRepository.Get(includeProperties: "Photo,ProductOccurences");
            var newList = new List<object>();
            foreach (var item in allProducts)
                newList.Add(new
                {
                    ProductId = item.ProductId,
                    ProductDescription = item.UnitOfMeasure + ": " + item.ProductDescription
                });
            var inputModel = new PurchasingCalculatorInputModel()
            {
                AllProducts = new SelectList(newList, "ProductId", "ProductDescription"),
                Criteria = TempData["currentCriteria"] as PurchasingCalculatorCriteriaModel
            };
            TempData["currentCriteria"] = inputModel.Criteria;
            return View(inputModel);
        }

        [HttpPost]
        public ActionResult SelectProducts(PurchasingCalculatorInputModel inputModel)
        {
            var criteria = TempData["currentCriteria"] as PurchasingCalculatorCriteriaModel;
            var productsWithAmount = new List<PurchasingCalculatorProductModel>();

            foreach (var selectedProductValue in inputModel.SelectedProductsValues)
            {
                Product product = unitOfWork.ProductRepository.GetById(selectedProductValue.SelectedProductId);
                productsWithAmount.Add(new PurchasingCalculatorProductModel(product, selectedProductValue.SelectedAmount));
            }

            if (ValidateSelectingProducts(inputModel, criteria.AmountOfProductFields))
            {
                var matchingProductOccurences = MatchProductOccurences(criteria);
                if (Request.Form["submitButtonShoppingBasket"] != null)
                    return View("CountShoppingBasket",
                        CountShoppingBasket(productsWithAmount, matchingProductOccurences));

                if (Request.Form["submitButtonInflation"] != null)
                    return View("CountInflation", CountInflation(productsWithAmount, matchingProductOccurences));

                if (Request.Form["submitButtonPurchasingPower"] != null)
                    return View("CountPurchasingPower",
                        CountPurchasingPower(productsWithAmount, matchingProductOccurences));
                return RedirectToAction("Index", "Home");
            }
            return View("ErrorCriteria");
        }

        public bool ValidateSelectingProducts(PurchasingCalculatorInputModel inputModel, int? amountOfProductFields)
        {
            for (var i=0; i<amountOfProductFields; i++)
            {
                if (inputModel.SelectedProductsValues[i].SelectedProductId == 0) return false;
            }
            return true;
        }

        public InflationModel CountInflation(IList<PurchasingCalculatorProductModel> products,
            List<ProductOccurence> matchingProductOccurences)
        {
            var shopInflation = CountInflationByShop(products, matchingProductOccurences);
            var productInflation = CountInflationByProduct(products, matchingProductOccurences);
            var averageInflation = CountInflationByProduct(products, matchingProductOccurences).Average(x => x.Inflation);
            return new InflationModel(shopInflation, productInflation, averageInflation);
        }

        public List<ShoppingBasketModel> CountShoppingBasket(IList<PurchasingCalculatorProductModel> products,
            List<ProductOccurence> matchingProductOccurences)
        {
            var shopsToCompare = SelectShopsToCompare(matchingProductOccurences);
            var shopsWithIncompleteBasket = 0;
            foreach (var shop in shopsToCompare)
            {
                decimal? counter = 0.00M;
                foreach (var product in products)
                {
                    var newestProductOccurence = FindNewestProductOccurence(matchingProductOccurences, shop, product);   
                    if (newestProductOccurence != null) counter = counter + (newestProductOccurence.ProductPrice * product.Amount);
                    else shop.ProductNotFound = true;
                }
                shop.BasketPrice = counter;
                if (shop.ProductNotFound) shopsWithIncompleteBasket++;
            }
            var results = shopsToCompare.Where(x => x.ProductNotFound == false).OrderBy(x => x.BasketPrice).ToList();
            if (shopsWithIncompleteBasket >= shopsToCompare.Count)
                ViewBag.BasketNotCompleteInAnyShop = true;
            else InitializeShoppingBasketChart(results);
            return results;
        }

        public List<InflationByShopModel> CountInflationByShop(IList<PurchasingCalculatorProductModel> products,
            List<ProductOccurence> matchingProductOccurences)
        {
            var shopsToConsider = SelectShopsToCompare(matchingProductOccurences);
            var shopInflations = new List<InflationByShopModel>();
            var shopsWithIncompleteBasket = 0;
            var isBasketComplete = true;

            foreach (var shop in shopsToConsider)
            {
                var productInflations = new List<decimal?>();
                foreach (var product in products)
                {
                    var newestProductOccurence = FindNewestProductOccurence(matchingProductOccurences, shop, product);
                    var oldestProductOccurence = FindOldestProductOccurence(matchingProductOccurences, shop, product);

                    if ((newestProductOccurence != null) && (oldestProductOccurence != null) && (newestProductOccurence != oldestProductOccurence))
                        productInflations.Add(CountInflationRate(newestProductOccurence, oldestProductOccurence));
                    else
                    {
                        shopsWithIncompleteBasket++;
                        isBasketComplete = false;
                        break;
                    }
                }
                if (isBasketComplete) shopInflations.Add(new InflationByShopModel(shop, productInflations.Average()));
            }
            if (shopsWithIncompleteBasket >= shopsToConsider.Count)
                ViewBag.BasketNotCompleteInAnyShop = true;
            else InitializeInflationByShopChart(shopInflations);
            return shopInflations.OrderBy(x => x.Inflation).ToList();
        }

        public List<InflationByProductModel> CountInflationByProduct(IList<PurchasingCalculatorProductModel> products,
            List<ProductOccurence> matchingProductOccurences)
        {
            var shopsToConsider = SelectShopsToCompare(matchingProductOccurences);
            var productInflations = new List<InflationByProductModel>();
            var productsNotFoundInShop = 0;
            var isBasketComplete = true;

            foreach (var product in products)
            {
                var shopInflations = new List<decimal?>();
                foreach (var shop in shopsToConsider)
                {
                    var newestProductOccurence = FindNewestProductOccurence(matchingProductOccurences, shop, product);
                    var oldestProductOccurence = FindOldestProductOccurence(matchingProductOccurences, shop, product);

                    if ((newestProductOccurence != null) && (oldestProductOccurence != null) && (newestProductOccurence != oldestProductOccurence))
                        shopInflations.Add(CountInflationRate(newestProductOccurence, oldestProductOccurence));
                    else
                    {
                        productsNotFoundInShop++;
                        isBasketComplete = false;
                        break;
                    }
                }
                if (isBasketComplete) productInflations.Add(new InflationByProductModel(product, shopInflations.Average()));
            }
            if (productsNotFoundInShop >= products.Count)
                ViewBag.NoProductFoundInAnyShop = true;
            else InitializeInflationByProductChart(productInflations);
            return productInflations.OrderBy(x => x.Inflation).ToList();
        }

        public List<PurchasingPowerModel> CountPurchasingPower(IList<PurchasingCalculatorProductModel> products,
            List<ProductOccurence> matchingProductOccurences)
        {
            var yearsToConsider = SelectYearsToCompare(matchingProductOccurences);
            var purchasingPowersAnnual = new List<PurchasingPowerModel>();

            foreach (var year in yearsToConsider)
            {
                var matchingProductOccurencesInYear = matchingProductOccurences.Where(x => x.ProductBuyDate.Year.Equals(year)).ToList();
                var shoppingBasketsInYear =
                    CountShoppingBasket(products, matchingProductOccurencesInYear);

                if (shoppingBasketsInYear.Any(x => x.BasketPrice > 0))
                {
                    var averageSalaryModel = new AverageSalaryModel(year);
                    var purchasingPowerValue = averageSalaryModel.Salary / ((shoppingBasketsInYear.Min(x => x.BasketPrice) + (shoppingBasketsInYear.Max(x => x.BasketPrice) / 2)));
                    purchasingPowersAnnual.Add(new PurchasingPowerModel(averageSalaryModel, purchasingPowerValue));
                }
            }
            InitializePurchasingPowerChart(purchasingPowersAnnual);
            return purchasingPowersAnnual;
        }

        public List<ProductOccurence> MatchProductOccurences(PurchasingCalculatorCriteriaModel criteria)
        {
            return unitOfWork.ProductOccurenceRepository.Get()
                    .Where(x => x.Shop.Addresses.Any(y => y.City.Contains(criteria.Localization)))
                    .Where(x => x.ProductBuyDate >= criteria.DateFrom)
                    .Where(x => x.ProductBuyDate <= criteria.DateTo)
                    .ToList();
        }

        public List<ShoppingBasketModel> SelectShopsToCompare(IEnumerable<ProductOccurence> matchingProductOccurences)
        {
            var shopsToCompare = new List<ShoppingBasketModel>();
            foreach (var shop in matchingProductOccurences.Select(p => p.Shop).Distinct())
            {
                var shopModel = new ShoppingBasketModel(shop, 0.00M);
                shopsToCompare.Add(shopModel);
            }
            return shopsToCompare;
        }

        public List<int> SelectYearsToCompare(IEnumerable<ProductOccurence> matchingProductOccurences)
        {
            var yearsToCompare = new List<int>();
            foreach (var year in matchingProductOccurences.Select(p => p.ProductBuyDate.Year).Distinct())
            {
                yearsToCompare.Add(year);
            }
            yearsToCompare.Sort();
            return yearsToCompare;
        }

        public ProductOccurence FindNewestProductOccurence(List<ProductOccurence> matchingProductOccurences,
            ShoppingBasketModel shoppingBasketModel, PurchasingCalculatorProductModel productModel)
        {
            return matchingProductOccurences
                .OrderByDescending(x => x.ProductBuyDate)
                .Where(x => x.Shop.ShopName == shoppingBasketModel.Shop.ShopName)
                .FirstOrDefault(x => x.ProductId == productModel.Product.ProductId);
        }

        public ProductOccurence FindOldestProductOccurence(List<ProductOccurence> matchingProductOccurences,
            ShoppingBasketModel shoppingBasketModel, PurchasingCalculatorProductModel productModel)
        {
            return matchingProductOccurences
                .OrderBy(x => x.ProductBuyDate)
                .Where(x => x.Shop.ShopName == shoppingBasketModel.Shop.ShopName)
                .FirstOrDefault(x => x.ProductId == productModel.Product.ProductId);
        }

        public decimal CountInflationRate(ProductOccurence newestProductOccurence, ProductOccurence oldestProductOccurence)
        {
            decimal productInflation;
            var dateDifference = (newestProductOccurence.ProductBuyDate - oldestProductOccurence.ProductBuyDate).TotalDays;
            var priceDifference = newestProductOccurence.ProductPrice - oldestProductOccurence.ProductPrice;

            if (dateDifference > 0) productInflation = ((priceDifference / oldestProductOccurence.ProductPrice) * 100) * (decimal)(365 / dateDifference);
            else productInflation = ((priceDifference / oldestProductOccurence.ProductPrice) * 100) * 365;
            return productInflation;
        }

        public void InitializeShoppingBasketChart(List<ShoppingBasketModel> listOfBaskets)
        {
            ViewBag.Labels = listOfBaskets.Select(x => x.Shop.ShopName).ToList();
            ViewBag.Values = listOfBaskets.Select(x => (double)x.BasketPrice).ToList();
        }

        public void InitializeInflationByShopChart(List<InflationByShopModel> listOfInflations)
        {
            ViewBag.ShopLabels = listOfInflations.Select(x => x.Shop.Shop.ShopName).ToList();
            ViewBag.ShopInflationValues = listOfInflations.Select(x => (double)x.Inflation).ToList();
        }
        public void InitializeInflationByProductChart(List<InflationByProductModel> listOfInflations)
        {
            ViewBag.ProductLabels = listOfInflations.Select(x => x.Product.Product.ProductDescription).ToList();
            ViewBag.ProductInflationValues = listOfInflations.Select(x => (double)x.Inflation).ToList();
        }

        public void InitializePurchasingPowerChart(List<PurchasingPowerModel> listOfPurchasingPowers)
        {
            ViewBag.Years = listOfPurchasingPowers.Select(x => x.AverageSalaryModel.Year.ToString()).ToList();
            ViewBag.PurchasingPowerValues = listOfPurchasingPowers.Select(x => (double)x.PurchasingPowerValue).ToList();
        }
    }
}