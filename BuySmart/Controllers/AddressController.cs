﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using BuySmart.DAL;
using BuySmart.Models;
using BuySmart.ViewModels;

namespace BuySmart.Controllers
{
    public class AddressController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public IEnumerable<Address> GetAddresses(AddressSearchModel searchModel)
        {
            var result = unitOfWork.AddressRepository.Get(includeProperties: "Shop");

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.City))
                    result = result.Where(x => x.City.Contains(searchModel.City));
                if (!string.IsNullOrEmpty(searchModel.Street))
                    result = result.Where(x => x.Street.Contains(searchModel.Street));
                if (!string.IsNullOrEmpty(searchModel.ShopName))
                    result = result.Where(x => x.Shop.ShopName.Contains(searchModel.ShopName));
                if (searchModel.ShopType.HasValue)
                    result = result.Where(x => x.Shop.ShopType.Equals(searchModel.ShopType));
            }
            return result;
        }

        private void PopulateShopsDropDownList(object selectedShop = null)
        {
            var shopQuery = unitOfWork.ShopRepository.Get(
                orderBy: q => q.OrderBy(d => d.ShopId));
            ViewBag.ShopID = new SelectList(shopQuery, "ShopId", "ShopName", selectedShop);
        }

        // GET: Address
        public ViewResult Index(string sortOrder, AddressSearchModel searchModel, string currentFilterCity, string currentFilterStreet,
            string currentFilterShopName, ShopType? currentFilterShopType, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.IDSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.CitySortParm = sortOrder == "city_asc" ? "city_desc" : "city_asc";
            ViewBag.StreetSortParm = sortOrder == "street_asc" ? "street_desc" : "street_asc";
            ViewBag.ShopNameSortParm = sortOrder == "shopName_asc" ? "shopName_desc" : "shopName_asc";
            ViewBag.ShopTypeSortParm = sortOrder == "shopType_asc" ? "shopType_desc" : "shopType_asc";

            if ((searchModel.City != null) || (searchModel.Street != null)
                || (searchModel.ShopName != null) || (searchModel.ShopType != null))
            {
                page = 1;
            }
            else
            {
                searchModel.City = currentFilterCity;
                searchModel.Street = currentFilterStreet;
                searchModel.ShopName = currentFilterShopName;
                searchModel.ShopType = currentFilterShopType;
            }

            var addresses = GetAddresses(searchModel);

            ViewBag.CurrentFilterCity = searchModel.City;
            ViewBag.CurrentFilterStreet = searchModel.Street;
            ViewBag.CurrentFilterShopName = searchModel.ShopName;
            ViewBag.CurrentFilterShopType = searchModel.ShopType;

            switch (sortOrder)
            {
                case "id_desc":
                    addresses = addresses.OrderByDescending(s => s.AddressId);
                    break;
                case "city_desc":
                    addresses = addresses.OrderByDescending(s => s.City);
                    break;
                case "city_asc":
                    addresses = addresses.OrderBy(s => s.City);
                    break;
                case "street_desc":
                    addresses = addresses.OrderByDescending(s => s.Street);
                    break;
                case "street_asc":
                    addresses = addresses.OrderBy(s => s.Street);
                    break;
                case "shopName_desc":
                    addresses = addresses.OrderByDescending(s => s.Shop.ShopName);
                    break;
                case "shopName_asc":
                    addresses = addresses.OrderBy(s => s.Shop.ShopName);
                    break;
                case "shopType_desc":
                    addresses = addresses.OrderByDescending(s => s.Shop.ShopType.ToString());
                    break;
                case "shopType_asc":
                    addresses = addresses.OrderBy(s => s.Shop.ShopType.ToString());
                    break;
                default:
                    addresses = addresses.OrderBy(s => s.AddressId);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            AddressIndexModel addressIndexModel = new AddressIndexModel(addresses, pageNumber, pageSize, currentFilterShopType);
            return View(addressIndexModel);
        }

        // GET: Address/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = unitOfWork.AddressRepository.GetById(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }
        
        // GET: Address/Create
        public ActionResult Create()
        {
            PopulateShopsDropDownList();
            return View();
        }

        // POST: Address/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShopId, Country, City, Street")]Address address)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.AddressRepository.Insert(address);
                    unitOfWork.Save();
                    return RedirectToAction("Index");
                }
                PopulateShopsDropDownList(address.ShopId);
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Zapisanie zmian zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.");
            }
            return View(address);
        }
      
        // GET: Address/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = unitOfWork.AddressRepository.GetById(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            PopulateShopsDropDownList(address.ShopId);
            return View(address);
        }

        // POST: Address/Edit/5
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var addressToUpdate = unitOfWork.AddressRepository.GetById(id);
            if (TryUpdateModel(addressToUpdate, "",
               new string[] { "ShopId", "Country", "City", "Street" }))
            {
                try
                {
                    unitOfWork.Save();
                    PopulateShopsDropDownList(addressToUpdate.ShopId);
                    return RedirectToAction("Index");
                }
                catch (DataException)
                {
                    ModelState.AddModelError("", "Zapisanie zmian zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.");
                }
            }
            return View(addressToUpdate);
        }

        // GET: Address/Delete/5
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Usuwanie zakończyło się niepowodzeniem. Spróbuj później, a jeśli problem będzie dalej występował, skontaktuj się z administratorem.";
            }
            Address address = unitOfWork.AddressRepository.GetById(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // POST: Address/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Address address = unitOfWork.AddressRepository.GetById(id);
                unitOfWork.AddressRepository.Delete(address);
                unitOfWork.Save();
            }
            catch (DataException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
