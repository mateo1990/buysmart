﻿using System.Web.Mvc;

namespace BuySmart.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Offer()
        {
            ViewBag.Message = "Co oferujemy?";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "O aplikacji BuySmart";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Strona kontaktowa";
            return View();
        }
    }
}