﻿using BuySmart.Models;

namespace BuySmart.ViewModels
{
    public class ProductSearchModel
    {
        public string Description { get; set; }
        public UnitOfMeasure? UnitOfMeasure { get; set; }
    }
}