﻿using System.Collections.Generic;
using BuySmart.Models;
using PagedList;

namespace BuySmart.ViewModels
{
    public class ProductIndexModel
    {
        public ProductIndexModel(IEnumerable<Product> products, int pageNumber, int pageSize, UnitOfMeasure? unitOfMeasure)
        {
            Products = products.ToPagedList(pageNumber, pageSize);
            UnitOfMeasure = unitOfMeasure;
        }

        public IPagedList<Product> Products { get; set; }
        public UnitOfMeasure? UnitOfMeasure { get; set; }
    }
}