﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuySmart.Models;
using BuySmart.ViewModels;

namespace BuySmart.ViewModels.PurchasingCalculator
{
    public class InflationByShopModel
    {
        public ShoppingBasketModel Shop { get; set; }
        public decimal? Inflation { get; set; }

        public InflationByShopModel(ShoppingBasketModel shop, decimal? inflation)
        {
            Shop = shop;
            Inflation = inflation;
        }
    }
}