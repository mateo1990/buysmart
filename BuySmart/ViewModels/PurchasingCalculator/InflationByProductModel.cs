﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuySmart.Models;

namespace BuySmart.ViewModels.PurchasingCalculator
{
    public class InflationByProductModel
    {
        public PurchasingCalculatorProductModel Product { get; set; }
        public decimal? Inflation { get; set; }

        public InflationByProductModel(PurchasingCalculatorProductModel product, decimal? inflation)
        {
            Product = product;
            Inflation = inflation;
        }
    }
}