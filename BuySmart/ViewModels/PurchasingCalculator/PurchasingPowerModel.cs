﻿
namespace BuySmart.ViewModels.PurchasingCalculator
{
    public class PurchasingPowerModel
    {
        public PurchasingPowerModel(AverageSalaryModel averageSalaryModel, decimal? purchasingPowerValue)
        {
            AverageSalaryModel = averageSalaryModel;
            PurchasingPowerValue = purchasingPowerValue;
        }

        public AverageSalaryModel AverageSalaryModel { get; set; }
        public decimal? PurchasingPowerValue { get; set; }
    }
}