﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuySmart.Models;

namespace BuySmart.ViewModels.PurchasingCalculator
{
    public class PurchasingCalculatorProductModel
    {

        public PurchasingCalculatorProductModel(Product product, decimal? amount)
        {
            Product = product;
            Amount = amount;
        }

        public Product Product { get; set; }
        public decimal? Amount { get; set; }
    }
}