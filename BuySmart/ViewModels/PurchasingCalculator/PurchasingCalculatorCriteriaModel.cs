﻿using System;

namespace BuySmart.ViewModels.PurchasingCalculator
{
    public class PurchasingCalculatorCriteriaModel
    {
        public int? AmountOfProductFields { get; set; }
        public string Localization { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}