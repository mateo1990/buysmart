﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuySmart.ViewModels.PurchasingCalculator
{
    public class InflationModel
    {
        public List<InflationByShopModel> ShopInflation { get; set; }
        public List<InflationByProductModel> ProductInflation { get; set; }
        public decimal? AverageInflation { get; set; }

        public InflationModel(List<InflationByShopModel> shopInflation, List<InflationByProductModel> productInflation,
            decimal? averageInflation)
        {
            ShopInflation = shopInflation;
            ProductInflation = productInflation;
            AverageInflation = averageInflation;
        }
    }
}