﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuySmart.Models;

namespace BuySmart.ViewModels.PurchasingCalculator
{
    public class ShoppingBasketModel
    {
        public ShoppingBasketModel(Shop shop, decimal? basketPrice)
        {
            Shop = shop;
            BasketPrice = basketPrice;
        }

        public Shop Shop { get; set; }
        public decimal? BasketPrice { get; set; }
        public bool ProductNotFound { get; set; }
    }
}