﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuySmart.ViewModels.PurchasingCalculator
{
    public class AverageSalaryModel
    {
        public int Year { get; set; }
        public decimal Salary { get; set; }

        public AverageSalaryModel(int year)
        {
            Year = year;
            Salary = GetAverageSalary(year);
        }

        public decimal GetAverageSalary(int year)
        {
            switch (year)
            {
                case 2017:
                    return 4257.79M;
                case 2016:
                    return 4055.04M;
                case 2015:
                    return 3899.78M;
                case 2014:
                    return 3783.46M;
                case 2013:
                    return 3650.06M;
                default:
                    return 0;
            }
        }

    }
}