﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace BuySmart.ViewModels.PurchasingCalculator
{
    public class PurchasingCalculatorInputModel
    {
        public PurchasingCalculatorCriteriaModel Criteria { get; set; }
        public PurchasingCalculatorSelectedProductValuesModel[] SelectedProductsValues { get; set; }
        public IEnumerable<SelectListItem> AllProducts { get; set; }
    }
}