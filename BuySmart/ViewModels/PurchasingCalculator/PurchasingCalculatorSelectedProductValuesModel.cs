﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuySmart.Models;

namespace BuySmart.ViewModels.PurchasingCalculator
{
    public class PurchasingCalculatorSelectedProductValuesModel : Product
    {
        public int SelectedProductId { get; set; }
        public int SelectedAmount { get; set; }
    }
}