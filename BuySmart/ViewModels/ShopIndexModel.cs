﻿using System.Collections.Generic;
using BuySmart.Models;
using PagedList;

namespace BuySmart.ViewModels
{
    public class ShopIndexModel
    {
        public ShopIndexModel(IEnumerable<Shop> shops, int pageNumber, int pageSize, ShopType? shopType)
        {
            Shops = shops.ToPagedList(pageNumber, pageSize);
            ShopType = shopType;
        }

        public IPagedList<Shop> Shops { get; set; }
        public ShopType? ShopType { get; set; }
    }
}