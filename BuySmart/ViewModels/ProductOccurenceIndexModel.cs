﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BuySmart.Models;
using PagedList;

namespace BuySmart.ViewModels
{
    public class ProductOccurenceIndexModel
    {
        public ProductOccurenceIndexModel(IEnumerable<ProductOccurence> productOccurences, int pageNumber, int pageSize,
            decimal? priceFrom, decimal? priceTo, DateTime? buyDateFrom, DateTime? buyDateTo)
        {
            ProductOccurences = productOccurences.ToPagedList(pageNumber, pageSize);
            PriceFrom = priceFrom;
            PriceTo = priceTo;
            BuyDateFrom = buyDateFrom;
            BuyDateTo = buyDateTo;
        }

        public IPagedList<ProductOccurence> ProductOccurences { get; set; }
        public decimal? PriceFrom;
        public decimal? PriceTo;

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? BuyDateFrom;

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? BuyDateTo;
    }
}