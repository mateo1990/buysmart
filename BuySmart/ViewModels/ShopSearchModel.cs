﻿using BuySmart.Models;

namespace BuySmart.ViewModels
{
    public class ShopSearchModel
    {
        public string Name { get; set; }
        public ShopType? ShopType { get; set; }
    }
}