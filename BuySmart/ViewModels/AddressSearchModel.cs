﻿using BuySmart.Models;

namespace BuySmart.ViewModels
{
    public class AddressSearchModel
    {
        public string City { get; set; }
        public string Street { get; set; }
        public string ShopName { get; set; }
        public ShopType? ShopType { get; set; }
    }
}