﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BuySmart.ViewModels
{
    public class ProductOccurenceSearchModel
    {
        public string Description { get; set; }
        public string ShopName { get; set; }

        public decimal? PriceFrom { get; set; }
        public decimal? PriceTo { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? BuyDateFrom { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? BuyDateTo { get; set; }

        public bool? IsPromotional { get; set; }
    }
}