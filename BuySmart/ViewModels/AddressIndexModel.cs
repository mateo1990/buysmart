﻿using System.Collections.Generic;
using BuySmart.Models;
using PagedList;

namespace BuySmart.ViewModels
{
    public class AddressIndexModel
    {
        public AddressIndexModel(IEnumerable<Address> addresses, int pageNumber, int pageSize, ShopType? shopType)
        {
            Addresses = addresses.ToPagedList(pageNumber, pageSize);
            ShopType = shopType;
        }

        public IPagedList<Address> Addresses { get; set; }
        public ShopType? ShopType { get; set; }
    }
}