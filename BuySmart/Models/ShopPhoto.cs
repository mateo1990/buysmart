﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BuySmart.Models
{
    public class ShopPhoto
    {
        [Key, ForeignKey("Shop")]
        public int ShopPhotoId { get; set; }
        public int ShopId { get; set; }

        [StringLength(255)]
        public string FileName { get; set; }
        [StringLength(100)]
        public string ContentType { get; set; }
        public byte[] Content { get; set; }

        public virtual Shop Shop { get; set; }
    }
}