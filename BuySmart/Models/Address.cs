﻿using System.ComponentModel.DataAnnotations;

namespace BuySmart.Models
{
    public class Address
    {
        public Address() {

        }

        [Key]
        public int AddressId { get; set; }
        public int ShopId { get; set; }
        [Required(ErrorMessage = "Miasto jest wymagane")]
        public string City { get; set; }
        [Required(ErrorMessage = "Ulica jest wymagana")]
        public string Street { get; set; }

        public virtual Shop Shop { get; set; }
    }
}