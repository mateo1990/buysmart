﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace BuySmart.Models
{
    public enum UnitOfMeasure
    {
        Item,
        Kilogram,
        Liter,
    }

    public class Product
    {
        public Product()
        {
            ProductOccurences = new List<ProductOccurence>();
        }

        [Key]
        public int ProductId { get; set; }
        public int ProductPhotoId { get; set; }

        public UnitOfMeasure? UnitOfMeasure { get; set; }
        [Required(ErrorMessage = "Opis produktu jest wymagany")]
        public string ProductDescription { get; set; }

        public virtual ProductPhoto Photo { get; set; }
        public virtual ICollection<ProductOccurence> ProductOccurences { get; set; }
    }

}