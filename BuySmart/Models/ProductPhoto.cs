﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BuySmart.Models
{
    public class ProductPhoto
    {
        [Key, ForeignKey("Product")]
        public int ProductPhotoId { get; set; }
        public int ProductId { get; set; }

        [StringLength(255)]
        public string FileName { get; set; }
        [StringLength(100)]
        public string ContentType { get; set; }
        public byte[] Content { get; set; }

        public virtual Product Product { get; set; }
    }
}