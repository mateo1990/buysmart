﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BuySmart.Models
{
    public class ProductOccurence
    {
        [Key]
        public int ProductOccurenceId { get; set; }
        public int ProductId { get; set; }
        public int ShopId { get; set; }

        [Range(0, 1000, ErrorMessage = "Cena produktu jest wymagana i musi mieścić się w przedziale od 0 do 1000 PLN")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal ProductPrice { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Data zakupu produktu jest wymagana")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ProductBuyDate { get; set; }

        public bool IsPromotional { get; set; }

        public virtual Product Product { get; set; }
        public virtual Shop Shop { get; set; }
    }
}