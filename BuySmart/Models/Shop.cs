﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BuySmart.Models
{
    public enum ShopType
    {
        UsualShop,
        Discount,
        SuperMarket,
        HyperMarket,
        Delicatessen,
        DrugStore,
        LiquorStore
    }

    public class Shop
    {
        public Shop()
        {
            ProductOccurences = new List<ProductOccurence>();
            Addresses = new List<Address>();
        }

        [Key]
        public int ShopId { get; set; }
        public int ShopPhotoId { get; set; }

        [Required(ErrorMessage = "Nazwa sklepu jest wymagana")]
        public string ShopName { get; set; }
        public ShopType? ShopType { get; set; }

        public virtual ShopPhoto Photo { get; set; }
        public virtual ICollection<ProductOccurence> ProductOccurences { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
    }
}